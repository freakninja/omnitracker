#                             OMNITRACKER
##                           a freak.ninja project
                 
 Omnitracker is a cookieless tracker, relays on CORs and uses ETAGs
 
 Apache configurations are available under configs/ directory.

# DISCLAIMER
This is not a clone & use software, you need to adapt it to your needs.
FreakNinja dislike cut & paste kiddies.

# INSTALLATION
 
    git clone git@bitbucket.org:freakninja/omnitracker.git /var/www/omnitracker
    cd /var/www/omnitracker
    cp configs/apache/{2.2|2.4}/* /path/to/apache/vhosts/folder/
 
 point test.omnitracker.int and ominitracker.int to the ip of the server.
 
# FOLDERS
| Folder    | Description   |
|----------:|--------------:|
| sessions/ | user sessions |
| public/ | user entry point, javascript client, test website |
| library/ | PHP Core library version |

--

Enjoy,

v@freak.ninja