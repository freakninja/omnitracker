<?php
namespace Omnitracker;

/**
 * Cookiless Session Tracker 
 * 
 * @link http://omnitracker.freak.ninja/
 * @author valerio tesei <v@freak.ninja>
 * @license GNU/GPL v2 
 * @version 1.0
 */
class Tracker
{
	private $_GUID = 'change_this_to_a_secret_value';
	
	/**
	 * Session eTAG
	 * 
	 * @var string[18]
	 */
	public $eTag = null;
	
	/**
	 * Session data
	 * 
	 * @var array
	 */
	private $_session = array();
	
	/**
	 * session data full path
	 * 
	 * @var string
	 */
	private $_session_path = null;
	
	/**
	 * full path to session file
	 * 
	 * @var string
	 */
	private $_session_file = null;
	
	/**
	 * Session expiry in seconds
	 * 
	 * @default 15552000 (6 Months)
	 * @var int
	 */
	private $_session_expiry = 15552000;
	
	/**
	 * 1x1 transparent gif content
	 * 
	 * @var binary
	 */
	private $_pixel_content = "\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x80\x00\x00\x00\x00\x00\xff\xff\xff\x21\xf9\x04\x01\x00\x00\x00\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x01\x44\x00\x3b";	

	/**
	 * 1x1 transparent gif size
	 * 
	 * @var int
	 */
	private $_pixel_size    = 42;
	
	/**
	 * Log levels:
	 * 
	 * DEBUG, INFO, ERROR, NONE
	 * 
	 * @default NONE
	 * @var string
	 */
	private $_logLevel = 'NONE';
	
	/**
	 * Log file location
	 * 
	 * @default NULL
	 * @var string
	 */
	private $_logFile = null;
	
	/**
	 * Response type : session, collect, cache
	 * 
	 * @var string
	 */
	private $_responseType = null;
	
	/**
	 * TRUE if session has been tracked.
	 * 
	 * @var bool
	 */
	private $_tracked = false;
	/**
	 * Create a new session object
	 *  
	 * @param string $session_path full path to session stored values (default './sessions' )
	 * @param string $pixel full path to reply pixel image (default '1x1.gif' )
	 * @param int $expiry session expiry in seconds (default 6 months [15552000])
	 */
	public function __construct($session_path = './sessions', $expiry = 15552000)
	{
		
		if($expiry !== null && $expiry > 0)
			$this->_session_expiry = $expiry;
		
		// check sessions folder
		$session_path = realpath($session_path) . '/';
		
		// create the folder if not exists	
		if(!file_exists($session_path)) {
			mkdir($session_path, 0700, true);
		}
		// check if the folder exists
		if(!is_dir($session_path)) {
			throw new \Exception("Session not created, good bye", 500);
		}
		
		// check if we can write into the folder
		if( !is_writable($session_path)  ) {
			throw new \Exception("Session folder not writable", 500);
		}
		
		$this->_session_path = $session_path;
	}
	
	public function track()
	{
		$this->_log(__FUNCTION__,"Called", 'DEBUG');
		
		// default responseType is session
		$this->_responseType = 'session';
		
		$this->_log(__FUNCTION__,"Request from {$_SERVER["REMOTE_ADDR"]} [{$_SERVER["HTTP_USER_AGENT"]}]", 'DEBUG');
		
		if (!empty($_SERVER["HTTP_IF_NONE_MATCH"])) { 
			// this means the user returned to the website
			$this->_log(__FUNCTION__,"found HTTP_IF_NONE_MATCH = {$_SERVER["HTTP_IF_NONE_MATCH"]}", 'DEBUG');
			// generate the etag
			$this->eTag = $this->_eTag($_SERVER["HTTP_IF_NONE_MATCH"]);
		} elseif(isset($_REQUEST['X-Omnitracker-User'])) { 
			// this means it is a collect request
			$this->_responseType = 'collect';
			$this->_log(__FUNCTION__,"found X-Omnitracker-User = {$_REQUEST['X-Omnitracker-User']}", 'DEBUG');
			$this->eTag = $this->_eTag($_REQUEST['X-Omnitracker-User']); // setting the etag to the user one.
		} else { 
			// if we are here it is a new request
			$this->eTag = $this->_eTag();
			$this->_log(__FUNCTION__,"New session, eTag = {$this->eTag}", 'DEBUG');
		}

		// store the session filename
		$this->_session_file = $this->_session_path . $this->eTag ;
		
		$this->_log(__FUNCTION__,"Session file : {$this->_session_file}", 'INFO');
		
		if( !file_exists($this->_session_file) ) { 
			// if the file does not exists is a new session
			$this->_initSession();
		} else if ($this->_responseType == 'collect'){ 
			// if the response type is collect means we have to track
			$this->_session = json_decode(file_get_contents($this->_session_file), true);
			$this->_updateSession();
		} else { 
			// all good, browser need a cache response, no action needed
			$this->_responseType = 'cache';
		}
		$this->_log(__FUNCTION__,"Return", 'DEBUG');
		
		$this->_save();
		
		$this->_tracked = true;
		
		return $this;
	}
	
	private function _eTag($etag = null)
	{
		if($etag != null ) {
			return substr(str_replace(".", "", str_replace("/", "", str_replace("\\", "", $etag))), 0, 18);
		} else {
			return substr(sha1($this->_GUID . sha1($_SERVER["REMOTE_ADDR"]) . sha1($_SERVER["HTTP_USER_AGENT"]) . sha1(time())), 0, 18);
		} 
	}
	
	public function logFile($logFile, $logLevel = 'INFO')
	{
		// logfiles, for debug or tuning
		if($logFile != null && @touch($logFile))
			$this->_logFile = $logFile;
		
		if( $logLevel != null && in_array($logLevel, array('DEBUG', 'INFO', 'ERROR', 'NONE')) )
			$this->_logLevel = $logLevel;
		
		return $this;
	}
	
	
	/**
	 * log to a logfile
	 * 
	 * @param string $fn
	 * @param string $m
	 * @param string $level
	 */
	private function _log($fn, $m, $level = 'INFO')
	{
		if ($this->_logLevel == 'NONE')
			return;
		
		if(!is_writable($this->_logFile))
			return;
		
		if( ($this->_logLevel == 'INFO' || $this->_logLevel == 'ERROR') && $level == 'DEBUG')
			return;

		if( $this->_logLevel == 'ERROR' && $level == 'INFO')
			return;
		
		$session = $this->eTag == null ? '000000000000000000' : $this->eTag; 
		
		$logline = sprintf("%s\t%s\t[%s]\t%s::%s\t%s\n", date('YmdHis'), $session , $level, __CLASS__ , $fn, $m );
		
		file_put_contents($this->_logFile, $logline, FILE_APPEND);
		
		return;
	}
	
	/**
	 * return the current session content
	 * 
	 * @return array
	 */
	public function getSession()
	{
		return $this->_session;
	}
	
	/**
	 * Initialize a new session
	 */
	private function _initSession()
	{
		$this->_log(__FUNCTION__,"Called", 'DEBUG');
		$now = time();
		// initialize the session array		
		$this->_session['user']        = $this->eTag; // same as X-Omnitracker-User
		$this->_session['first_visit'] = $now;
		$this->_session['last_visit']  = $now;
		$this->_session['session']     = $this->_session['first_visit'] + $this->_session_expiry; // same as X-Omnitracker-Session
		$this->_session['expiry']      = $this->_session['session'];
		$this->_session['visits']      = 0; // no visits at this time, we just create the session structure
		$this->_session['counters']    = array('page_view'=>0, 'event'=> 0, 'goal' => 0, 'coversion' => 0 );
		$this->_session['sessions']    = array();
		$this->_log(__FUNCTION__,"New session created : {$this->eTag}", 'INFO');
		$this->_log(__FUNCTION__,"Return", 'DEBUG');
	}
	
	/**
	 * Update the current session
	 */
	private function _updateSession()
	{
		$this->_log(__FUNCTION__,"Called", 'DEBUG');
		
		// session expire [start]
		if($this->_session['expiry'] < time()) {
			$this->_log(__FUNCTION__,"Session expired : $this->_session['user']", 'DEBUG');
			$this->eTag = $this->_eTag();
			$this->_initSession();
		}
		// session expire [start]
		
		// update the session array
		$now = time();
		
		if($_REQUEST['__TYPE__'] == 'page_view' || !isset($_REQUEST['__TYPE__'])) {
			$_REQUEST['__TYPE__'] = 'page_view';
			$this->_session['last_visit']  = $now;
			$this->_session['visits']++;
		}
		
		if(isset($this->_session['counters'][$_REQUEST['__TYPE__']]))
			$this->_session['counters'][$_REQUEST['__TYPE__']]++;
		
		// log the request
		$_REQUEST['useragent'] = $_SERVER["HTTP_USER_AGENT"];
		@$this->_session['sessions'][$now] = $_REQUEST;
		$this->_log(__FUNCTION__,"Session updated, tracked '{$_REQUEST['__TYPE__']}' call, visits: {$this->_session['visits']}", 'INFO');
		$this->_log(__FUNCTION__,"Return", 'DEBUG');
	}
	
	/**
	 * Save the current session to file
	 * 
	 * @throws \Exception
	 * @return true
	 */
	private function _save()
	{
		$this->_log(__FUNCTION__,"Called", 'DEBUG');
		if($this->_responseType != 'cache') {
			$session_content = json_encode($this->_session, JSON_PRETTY_PRINT);
			if ( false === file_put_contents($this->_session_file, $session_content)) {
				$this->_log(__FUNCTION__,"Unable to save session file : {$this->_session_file}", 'ERROR');
				throw new \Exception("Unable to save session file", 500);
			}
		}
		$this->_log(__FUNCTION__,"Return", 'DEBUG');
		return true;
	}
	
	/**
	 * Destroy the current session to file
	 * 
	 * @throws \Exception
	 * @return true
	 */
	private function _destroy()
	{	
		$this->_log(__FUNCTION__,"Called", 'DEBUG');
		if ( file_exists($this->_session_file) ) {
			@unlink($this->_session_file);
		}
		$this->_log(__FUNCTION__,"Return", 'DEBUG');
		
		return true;
	}
	
	/**
	 * Format the session reply.
	 * NOTE: Calling this function terminate the script execution.
	 * 
	 */
	public function reply()
	{
		$this->_log(__FUNCTION__,"Called", 'DEBUG');
		
		if($this->_tracked == false) {
			$this->_log(__FUNCTION__,"track() function not called, calling it now.", 'DEBUG');
			$this->track();
		}
		
		$responseSize = 0;
		$responseBody = null;
		$this->_log(__FUNCTION__,"Reply type: {$this->_responseType}", 'INFO');
		
		if($this->_responseType == 'cache') {
			header("HTTP/1.1 304 Not Modified"); 
		} else {
			if($this->_responseType == 'session') {
				header("Content-type: text/javascript");
				$responseBody = json_encode(array('u'=>$this->eTag, 's'=>$this->_session['expiry']));
				$responseSize = strlen($responseBody);
				$this->_log(__FUNCTION__,"Session info: {$responseBody}", 'DEBUG');
			} else if ( $this->_responseType == 'collect') {
				header("Content-type: image/gif");
				$responseBody = $this->_pixel_content;
				$responseSize = $this->_pixel_size;
				$this->_log(__FUNCTION__,"Session info: GIF", 'DEBUG');
			}
			header("Cache-Control: private, must-revalidate, proxy-revalidate");
			header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', $this->_session['expiry']));
			header("ETag: {$this->_session['user']}"); // our "cookie"
			header("X-Omnitracker-User: " . $this->_session['user']); // our "usercode"
			header("X-Omnitracker-Session: " . $this->_session['expiry']); // our "session"
			header('Access-Control-Expose-Headers: X-Omnitracker-User, X-Omnitracker-Session');
			header("Content-length: " . $responseSize);
			print $responseBody;
		}
		$this->_log(__FUNCTION__,"Return", 'DEBUG');
		exit;
	}
}

/**
 * Thanks to
 * https://github.com/lucb1e/cookielesscookies/blob/master/index.php
 */