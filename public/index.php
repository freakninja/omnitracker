<?php
require_once('../library/Omnitracker/Tracker.php');

error_reporting(E_ALL);
ini_set('display_errors','1');
$st = new Omnitracker\Tracker('../sessions',null);
$st->logFile(realpath('../logs') . '/SessionTracker.log.' . date('Ymd'), 'DEBUG');
$st->track();
$session = $st->getSession();
// your session handling code here
$st->reply(); // terminate the exectution
