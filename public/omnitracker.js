"use strict";
(function (window, document, navigator, OmnitrackerQ){

	OmnitrackerQ = OmnitrackerQ || {};
	
	OmnitrackerQ.objectName = OmnitrackerQ.objectName || 'Omnitracker';
	
	// anonymous tracker object
	var OmnitrackerTracker = {
		__clone__ : function() {
			var o = {} ;
			for(var k in this) {
				if(k != '__clone__')
				o[k] = this[k];
			}
			return o;
		}
	};
	
	/**
	 * Called when a track event is done
	 */
	var fnTrackSuccess = function() { 
		if(OmnitrackerQ.fnTracked) OmnitrackerQ.fnTracked.call(this,OmnitrackerTracker.__TYPE__,OmnitrackerTracker.__clone__() ) ;
	};
	
	/**
	 * Called when a track event is failed
	 */
	var fnTrackFail = function() { 
		if(OmnitrackerQ.fnTrackFailed) OmnitrackerQ.fnTrackFailed.call(this,OmnitrackerTracker.__TYPE__,OmnitrackerTracker.__clone__() ) ;
	};
	
	/**
	 * Construct the object
	 */
	function Omnitracker()
	{
		this.pReady = false; 
		this.pFailed = false;
		if(window.XMLHttpRequest == undefined) {
			throw "Omnitracker : Unsupported platform";
		}
		
		OmnitrackerTracker.location  = window.location.toString();
		OmnitrackerTracker.referrer  = document.referrer.toString();
		OmnitrackerTracker.time      = (new Date()).toString();
		OmnitrackerTracker.screen    = this._screen();
		OmnitrackerTracker.plugins   = this._plugins();
		OmnitrackerTracker.language  = navigator.userLanguage || navigator.language;
		
		// find the server url
		var scripts = document.getElementsByTagName("script");
		for (var i = 0; i < scripts.length; i++) { 
		    var pattern = /omnitracker\.js/i; // the name of your js, whose source you are looking for
		    if ( pattern.test( scripts[i].getAttribute("src") ) ) {
		       this.sessionUrl = scripts[i].getAttribute("src").toString().replace('omnitracker.js', 'session.js'); //remove the omnitracker script filename
		       this.collectUrl   = scripts[i].getAttribute("src").toString().replace('omnitracker.js', 'collect.gif'); //remove the omnitracker script filename
		    }
		}
		
		if(OmnitrackerQ.useCookie && OmnitrackerQ.useCookie === true) {
			OmnitrackerTracker.trackMethod = 'cookie';
			this._cookie();
		} else {
			OmnitrackerTracker.trackMethod = 'cookieless';
			this._cookieLess();
		}
	};
	
	var OmnitrackerUtitlies = {
			Ready : function(fn, ctx)
			{
				if(ctx.pFailed === true) throw "Omnitracker tracker failed to fetch session, sorry about that.";
				if(ctx.pReady === true) return fn.call(ctx, fn);
				setTimeout(function(){
					OmnitrackerUtitlies.Ready(fn, ctx);
				}, 9);
			},
			JSONValidate : function (JSONString) {
				try {
					JSON.parse(JSONString);
					return true;
				}
				catch(e){
					return false;
				}
			}
	};
	
	/**
	 * Cookies
	 */
	OmnitrackerUtitlies.cookies = {
		/**
		 * read a cookie
		 * @param c_name string cookie name
		 * @param lowercase if true return the cookie content lowercase
		 * @return string cookie content
		 */
		read : function(c_name, lowercase){
			if (document.cookie.length>0) { 
				var c_start = document.cookie.indexOf(c_name+"=");
				if (c_start!=-1) {
					c_start	= c_start + c_name.length+1; 
					var c_end	= document.cookie.indexOf(";",c_start);
					if (c_end==-1) c_end=document.cookie.length;
	
					var cookie_to_use = unescape(document.cookie.substring(c_start,c_end));		
					if (lowercase) {
						return cookie_to_use.toLowerCase();
					}
					return cookie_to_use;
			    }
			}
			return '';
		},
		/**
		 * write a cookie
		 * @param c_name string cookie name
		 * @param c_value string cookie value
		 * @param expiry int expiry date in seconds
		 * @return void
		 */
		write : function(c_name, c_value, expiry) {
			debugger;
			var exdate=new Date(expiry*1000);
			c_value=escape(c_value) + "; expires=" +exdate.toUTCString() + ";domain="+window.location.host+";path=/";
			document.cookie=c_name + "=" + c_value;
		}
	};
	
	/**
	 * Execute a function when session has been retrieved from server
	 * 
	 * @param fn
	 * @returns void
	 */
	Omnitracker.prototype._fnReady = function(fn) {
		OmnitrackerUtitlies.Ready(fn, this);
	}
	
	Omnitracker.prototype.type = 'Omnitracker/x-Object';
	
	/**
	 * Provide Cookies functionalities
	 * 
	 */
	Omnitracker.prototype._cookie = function()
	{
		var self = this, cookie = OmnitrackerUtitlies.cookies.read('__omnitracker');		
		if(cookie) {
			OmnitrackerTracker['X-Omnitracker-User'] = cookie.split('.')[0];
			OmnitrackerTracker['X-Omnitracker-Session'] = cookie.split('.')[1];
			self.pReady = true;
		} else {
			var http = (new OmnitrackerHttp(this.sessionUrl, {})).get(this, function(data) {
				var userHeader = http.getHeader('X-Omnitracker-User'),
				    sessionHeader = http.getHeader('X-Omnitracker-Session');
			
				if(data || (userHeader && sessionHeader) ) {
					OmnitrackerTracker['X-Omnitracker-User'] = (userHeader) ? userHeader : data.u;
					OmnitrackerTracker['X-Omnitracker-Session'] = (sessionHeader) ? sessionHeader : data.s;
					OmnitrackerUtitlies.cookies.write('__omnitracker', OmnitrackerTracker['X-Omnitracker-User'] + '.' + OmnitrackerTracker['X-Omnitracker-Session'], OmnitrackerTracker['X-Omnitracker-Session']);
					self.pReady = true;
				}
			});
		}
	};
	
	/**
	 * CookieLess tracking mechanism
	 * 
	 */
	Omnitracker.prototype._cookieLess = function()
	{
		var self = this;
		var http = (new OmnitrackerHttp(this.sessionUrl, {})).get(this, function(data) {
			var userHeader = http.getHeader('X-Omnitracker-User'),
			    sessionHeader = http.getHeader('X-Omnitracker-Session');
		
			if(data || (userHeader && sessionHeader) ) {
				OmnitrackerTracker['X-Omnitracker-User'] = (userHeader) ? userHeader : data.u;
				OmnitrackerTracker['X-Omnitracker-Session'] = (sessionHeader) ? sessionHeader : data.s;
				self.pReady = true;
			}
		});
	};
	
	/**
	 * Collect the site information and parse the custom variables
	 * 
	 * @returns {void}
	 */
	Omnitracker.prototype._track = function()
	{
		
		if(OmnitrackerQ.cookiesTracking === true)
			OmnitrackerTracker.cookies = document.cookie;

		OmnitrackerTracker.__SITEID__ = 0;
		if(OmnitrackerQ.siteId) {
			OmnitrackerTracker.__SITEID__ = OmnitrackerQ.siteId;
			delete OmnitrackerQ.siteId;
		}
		if(OmnitrackerQ.customVars) {
			OmnitrackerTracker['cVars'] = {};
			for(var q in OmnitrackerQ.customVars ) {
				OmnitrackerTracker['cVars'][q] = OmnitrackerQ.customVars[q];
			}
		}
	};
	
	/**
	 * Fetch the Screen information and configuration
	 * 
	 * @returns {Array}
	 */
	Omnitracker.prototype._screen = function()
	{
		return { aH : window.screen.availHeight || window.screen.height, 
                 aW : window.screen.availWidth || window.screen.width, 
                 w : window.screen.width, 
                 h : window.screen.height, 
                 cd : window.screen.colorDepth || '' };
	};
	
	/**
	 * Fetch the browser plugins list
	 * 
	 * @returns {Array} [ '{plugin_name}:::{plugin_version}', ... ]
	 */
	Omnitracker.prototype._plugins = function()
	{
		var l = navigator.plugins.length, i, plugins = {} ;
		for(i = 0; i < l; i++ ) {
			plugins[navigator.plugins[i].name] = true;
		}
		return plugins;
	};
	
	/**
	 * Sets custom variable to be sent to the tracker
	 * 
	 * @param name
	 * @param value
	 * @returns Omnitracker/x-Object
	 */
	Omnitracker.prototype.setCustomVar = function(name, value)
	{
		OmnitrackerQ.customVars[name] = value;
		return this;
	};
	
	/**
	 * Get custom variable value
	 * 
	 * @param name
	 * @param value
	 */
	Omnitracker.prototype.getCustomVar = function(name)
	{
		if(OmnitrackerQ.customVars[name])
			return OmnitrackerQ.customVars[name];
	};
	
	/**
	 * Unset custom variable value
	 * 
	 * @param name
	 * @param value
	 */
	Omnitracker.prototype.unsetCustomVar = function(name)
	{
		if(OmnitrackerQ.customVars[name])
			delete OmnitrackerQ.customVars[name];
		return this;
	};
	
	/**
	 * Override a track object variable
	 * 
	 * @param name
	 * @param value
	 * @returns Omnitracker/x-Object
	 */
	Omnitracker.prototype.override = function(name, value)
	{
		if ( OmnitrackerTracker[this.guid][name] ) {
			OmnitrackerTracker[this.guid][name] = value;
		}
		return this;
	};
	
	/**
	 * Tracks page view
	 * 
	 * @returns {Omnitracker}
	 */
	Omnitracker.prototype.trackPageView = function()
	{
		this._fnReady(function(){
			this._track();
			OmnitrackerTracker.__TYPE__ = 'page_view';
			var http = new OmnitrackerHttp(this.collectUrl, OmnitrackerTracker);
			http.post(this, fnTrackSuccess, fnTrackFail);
		});
		return this;
	};
	
	/**
	 * Tracks a goal
	 * 
	 * @param name
	 * @param value
	 * @returns {Omnitracker}
	 */
	Omnitracker.prototype.trackGoal = function(name, value)
	{
		this._fnReady(function(){
			this._track();
			OmnitrackerTracker.__TYPE__ = 'goal';
			OmnitrackerTracker.goal = {} ;
			OmnitrackerTracker.goal[name] = value;
			var http = new OmnitrackerHttp(this.collectUrl, OmnitrackerTracker);
			http.post(this, fnTrackSuccess, fnTrackFail);
		});
		return this;
	};
	
	/**
	 * Tracks an event
	 * 
	 * @param category
	 * @param label
	 * @param value
	 * @returns {Omnitracker}
	 */
	Omnitracker.prototype.trackEvent = function(category, label, action, value)
	{
		if(!value) value = 0;
		this._fnReady(function(){
			this._track();
			OmnitrackerTracker.__TYPE__ = 'event';
			OmnitrackerTracker.event = { c : category, l : label , a : action , v : value};
			var http = new OmnitrackerHttp(this.collectUrl, OmnitrackerTracker);
			http.post(this, fnTrackSuccess, fnTrackFail);
		});
		return this;
	};
	
	/**
	 * Tracls a conversion
	 * 
	 * @param category
	 * @param label
	 * @param value
	 * @param moneyValue
	 * @returns {Omnitracker}
	 */
	Omnitracker.prototype.trackCoversion = function(category, label, value, moneyValue )
	{
		this._fnReady(function(){
			this._track();
			OmnitrackerTracker.__TYPE__  = 'coversion';
			OmnitrackerTracker.coversion = { c : category, l : label , v : value, $ : moneyValue };
			var http = new OmnitrackerHttp(this.collectUrl, OmnitrackerTracker);
			http.post(this, fnTrackSuccess, fnTrackFail);
		});
		return this;		
	};
	
	/**
	 * HTTP Post object 
	 * 
	 * @param url
	 * @param dataObject
	 */
	function OmnitrackerHttp(url, dataObject)
	{
		this.url      = url;
		this.postData =  this._serialize(dataObject);
	};
	
	/**
	 * Inititalize the XMLHttpRequest Object
	 * 
	 */
	OmnitrackerHttp.prototype._init = function(context, fnSuccessCallback, fnFailedCallback)
	{
		this.xhr = (window.XMLHttpRequest) ? new window.XMLHttpRequest : new ActiveXObject("Microsoft.XMLHTTP");
		
		if ( !this.xhr ) {
			throw "Omnitracker/Http : Cannot create xmlHttpRequest object";
		}
		
		if( typeof fnFailedCallback == 'function' ||  typeof fnSuccessCallback == 'function' ) { 
			this.xhr.onreadystatechange = function(xhr, data) {
				xhr = xhr.currentTarget;
				if(xhr.readyState == 4  && xhr.status == 200) {
					if( typeof fnSuccessCallback == 'function' ) {
						if ( this.getResponseHeader('content-type') == 'text/javascript' || 
								this.getResponseHeader('content-type') == 'application/javascript'  ) {
							if ( OmnitrackerUtitlies.JSONValidate(xhr.responseText)) {
								fnSuccessCallback.call(context,JSON.parse(xhr.responseText));
							} else {
								context.pFailed = true;
								if( typeof fnFailedCallback == 'function' ) fnFailedCallback.call(context,null);
							}
						} else if ( this.getResponseHeader('content-type') == 'text/html' ) {
							context.pFailed = true;
							if( typeof fnFailedCallback == 'function' ) fnFailedCallback.call(context,null);
						} else { 
							fnSuccessCallback.call(context,null);
						}
					}
				} else if (xhr.readyState == 4 ) {
					this.pFailed = true;
					if( typeof fnFailedCallback == 'function' ) {
						if ( this.getResponseHeader('content-type') == 'text/javascript' || 
								this.getResponseHeader('content-type') == 'application/javascript'  ) {
							fnFailedCallback.call(context,JSON.parse(xhr.responseText));
						} else { 
							fnFailedCallback.call(context,null);
						}
					}
				}
			};
		}
		return;
	};
	
	/**
	 * POST data, somewhere, somehow
	 * 
	 */
	OmnitrackerHttp.prototype.post = function(context, fnSuccessCallback, fnFailedCallback)
	{
		this._init(context, fnSuccessCallback, fnFailedCallback);
		this.xhr.open('POST', this.url, true);
		this.xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		this.xhr.send(this.postData);
		return this;
	};
	
	/**
	 * POST data, somewhere, somehow
	 * 
	 */
	OmnitrackerHttp.prototype.get = function(context, fnSuccessCallback, fnFailedCallback)
	{
		this._init(context, fnSuccessCallback, fnFailedCallback);
		this.xhr.open('GET', this.url, true);
		this.xhr.send(this.postData);
		return this;
	};
	
	/**
	 * Return the current etag for the resource.
	 * 
	 */
	OmnitrackerHttp.prototype.getHeader = function(header)
	{
		return this.xhr.getResponseHeader(header);
	}
	/**
	 * Recursively convert an object to querystring for POST or GET requests
	 * 
	 * @param obj
	 * @param prefix
	 * @returns
	 */
	OmnitrackerHttp.prototype._serialize = function(obj, prefix) 
	{
		var str = [];
		for ( var p in obj) {
			if( typeof obj[p] == 'function') continue;
			if (obj.hasOwnProperty(p)) {
				var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
				str.push(typeof v == "object" ? this._serialize(v, k)
						: encodeURIComponent(k) + "=" + encodeURIComponent(v));
			}
		}
		return str.join("&");
	};
	
	window[OmnitrackerQ.objectName] = new Omnitracker();

	if(OmnitrackerQ.trackPageView !== false )
		window[OmnitrackerQ.objectName].trackPageView();
	
}(window, document, (navigator || window.navigator) , window['OmnitrackerQ']));